#!/bin/bash
apt-get install -y python-dev python-pip
pip install -r server/deploy/conf/requirements.txt
supervisorctl restart omegalpes-web-back-end