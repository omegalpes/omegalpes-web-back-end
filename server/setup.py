import os
from setuptools import find_packages, setup


# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='omegalpes-web-back-end',
    version="0.1.0", 
    packages=['omegalpes_web'],
    package_dir={'omegalpes_web':'api'},
    include_package_data=True,
    license='Unknown',  # TODO
    description='OMEGAlpes',
    long_description="OMEGAlpes",
    url='http://www.g2elab.grenoble-inp.fr/recherche/omegalpes',
    author='G2Elab',
    author_email='omegalpes@grenoble-inp.fr',
    install_requires=['django-cors-headers>=2.2.0',
        'djangorestframework>=3.9.0',
        'autopep8>=1.4.3',
        'omegalpes==0.2.2',
        'jsonschema>=2.6.0',
        'yamale==2.0.1',
        'python-Levenshtein==0.12.0'],
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 1.11',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
