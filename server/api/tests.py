from django.test import SimpleTestCase
from django.urls import path, include, reverse
from rest_framework import status
from rest_framework.test import APITestCase, URLPatternsTestCase
from pathlib import Path
import yaml
from .views import check_grammar_validity, get_units_from_yaml, build_json_response
import json

class ApiTestCase(SimpleTestCase):
    urlpatterns = [
        path('', include('api.urls')),
    ]

    def test_api_end_point(self):
        url = reverse('config-file')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

#class ClassesTestCase(SimpleTestCase):
#
#    def setUp(self):
#        
