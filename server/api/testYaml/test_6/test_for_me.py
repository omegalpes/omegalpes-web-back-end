

#### Test 1 -> read parameters and objectives
omegalpes_version: 2.1
model:
  parameters:
    - name: periods
      label: Periods
      type: number
      default: 24
    - name: dt
      label: Delta T (hours)
      type: number
      default: 1
  energies:
    - energy: Gas
      color: "#b7becd"
    - energy: Electricity
      color: "#335bff"
    - energy: Heat
      color: "#ff5533"
classes:
#===========================ENERGY UNIT
  - class_name: EnergyUnit
    displayable: false
    parameters:
      - name: p
        label: Power demand
        type: vector
        default: null
      - name: name
        label: name
        type: string
    objectives:
      - name: minimize_starting_cost
        label: Minimize Starting Cost



###### Test 2 -> extends parameters and objectives


omegalpes_version: 2.1
model:
  parameters:
    - name: periods
      label: Periods
      type: number
      default: 24
    - name: dt
      label: Delta T (hours)
      type: number
      default: 1
  energies:
    - energy: Gas
      color: "#b7becd"
    - energy: Electricity
      color: "#335bff"
    - energy: Heat
      color: "#ff5533"
classes:
#===========================ENERGY UNIT
  - class_name: EnergyUnit
    displayable: false
    parameters:
      - name: p
        label: Power demand
        type: vector
        default: null
      - name: name
        label: name
        type: string
      - name: p_min
        label: Minimal Power demand
        type: number
        default: null
    objectives:
      - name: minimize_starting_cost
        label: Minimize Starting Cost
  - class_name: FixedEnergyUnit
    displayable: false
    type: Fixed
    extends:
      - EnergyUnit
  - class_name: VariableEnergyUnit
    displayable: false
    type: Variable
    extends:
      - EnergyUnit


###### Test 3 -> StorageUnit extends EnergyUnit without parameters_extended ans hide_parent_objectives


omegalpes_version: 2.1
model:
  parameters:
    - name: periods
      label: Periods
      type: number
      default: 24
    - name: dt
      label: Delta T (hours)
      type: number
      default: 1
  energies:
    - energy: Gas
      color: "#b7becd"
    - energy: Electricity
      color: "#335bff"
    - energy: Heat
      color: "#ff5533"
classes:
#===========================ENERGY UNIT
  - class_name: EnergyUnit
    displayable: false
    parameters:
      - name: p
        label: Power demand
        type: vector
        default: null
      - name: name
        label: name
        type: string
    objectives:
      - name: minimize_starting_cost
        label: Minimize Starting Cost
      - name: minimize_operating_cost
        label: Minimize Operating Cost
  - class_name: StorageUnit
    extends:
      - VariableEnergyUnit
    label: Storage
    type: Storage
    parameters:
      - name: pc_min
        label: Charging Power min
        type: number
        default: null
    objectives:
      - name: minimize_capacity
        label: Minimize Capacity
  - class_name: VariableEnergyUnit
    displayable: false
    type: Variable
    extends:
      - EnergyUnit


###### Test 4 -> StorageUnit extends EnergyUnit and use parameters_extended


omegalpes_version: 2.1
model:
  parameters:
    - name: periods
      label: Periods
      type: number
      default: 24
    - name: dt
      label: Delta T (hours)
      type: number
      default: 1
  energies:
    - energy: Gas
      color: "#b7becd"
    - energy: Electricity
      color: "#335bff"
    - energy: Heat
      color: "#ff5533"
classes:
#===========================ENERGY UNIT
  - class_name: EnergyUnit
    displayable: false
    parameters:
      - name: p
        label: Power demand
        type: vector
        default: null
      - name: name
        label: name
        type: string
    objectives:
      - name: minimize_starting_cost
        label: Minimize Starting Cost
      - name: minimize_operating_cost
        label: Minimize Operating Cost
  - class_name: StorageUnit
    extends:
      - VariableEnergyUnit
    label: Storage
    type: Storage
    parameters_extended:
      - name
      - energy_type
      - e_min
      - e_max
    parameters:
      - name: pc_min
        label: Charging Power min
        type: number
        default: null
    objectives:
      - name: minimize_capacity
        label: Minimize Capacity
  - class_name: VariableEnergyUnit
    displayable: false
    type: Variable
    extends:
      - EnergyUnit

###### Test 5 -> StorageUnit extends EnergyUnit and use hide_parent_objectives


omegalpes_version: 2.1
model:
  parameters:
    - name: periods
      label: Periods
      type: number
      default: 24
    - name: dt
      label: Delta T (hours)
      type: number
      default: 1
  energies:
    - energy: Gas
      color: "#b7becd"
    - energy: Electricity
      color: "#335bff"
    - energy: Heat
      color: "#ff5533"
classes:
#===========================ENERGY UNIT
  - class_name: EnergyUnit
    displayable: false
    parameters:
      - name: p
        label: Power demand
        type: vector
        default: null
      - name: name
        label: name
        type: string
    objectives:
      - name: minimize_starting_cost
        label: Minimize Starting Cost
      - name: minimize_operating_cost
        label: Minimize Operating Cost
  - class_name: StorageUnit
    extends:
      - EnergyUnit
    label: Storage
    type: Storage
    parameters:
      - name: pc_min
        label: Charging Power min
        type: number
        default: null
    objectives:
      - name: minimize_capacity
        label: Minimize Capacity
    hide_parent_objectives:
      - minimize_starting_cost
