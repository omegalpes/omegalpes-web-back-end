from wsgiref.util import FileWrapper

from django.http import HttpResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
import yaml
import os
from pathlib import Path
import inspect

from .helper.generate_script import generate_python_script
import omegalpes.energy.units
import pkgutil
import importlib
import re
import Levenshtein
import copy
import functools

from .helper.unit import Unit, Parameter, Objective, DisjointObjective, Constraint, unique_array, unique_parameters
from .helper.classes import Classe

from django.core.cache import cache
from django.core import serializers

from jsonschema import validate
import yaml
import yamale
from yamale.validators import DefaultValidators, Validator
import logging

logger = logging.getLogger(__name__)
# Class Default used for yamale validator
class Default(Validator):
    """ Custom Date validator """
    tag = 'default'

    # _is_valid return True if value is a string or an int
    def _is_valid(self, value):
        return isinstance(value, str) or isinstance(value, int)


levenshtein_max_distance = 3
# config_file returns the configuration depending on the api/config/config.yaml
@api_view(['GET'])
def config_file(request):
    if request.method == 'GET':
        ## Si objects en cache, les prendre, sinon refaire ça.
        folder = Path('api/config/')
        yaml_file = folder / 'config.yaml'
        with open(yaml_file, 'r') as f:
            yaml_json_object = yaml.load(f, Loader=yaml.UnsafeLoader)
            units = get_units_from_yaml(copy.deepcopy(yaml_json_object))
            if is_coherent(units, yaml_json_object):
                return Response(status=status.HTTP_200_OK, data=build_json_response(units, yaml_json_object))
            else:
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data='Not coherent json response')
    else:
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


# configtest returns the errors of the file api/config/test_yaml/config.yaml
@api_view(['GET'])
def configtest(request):
    if request.method == 'GET':
        validate_structure('api/config/config.yaml')
        classes = get_classes('api/config/test_yaml/config.yaml')

        folder = Path('api/config/test_yaml')
        yaml_file = folder / 'config.yaml'
        with open(yaml_file, 'r') as f:
            yaml_json_object = yaml.load(f, Loader=yaml.UnsafeLoader)
            errors = get_error_from(yaml_json_object, classes)
            is_grammar_ok = errors == []
            if is_grammar_ok:
                return Response(status=status.HTTP_200_OK, data='Le fichier api/config/test_yaml/config.yaml est correct')
            else:
                return Response(status=status.HTTP_200_OK, data=errors)


@api_view(['POST'])
def python_script(request):
    if request.method == 'POST':
        json_to_transform_to_python = request.data
        script_path_file = generate_python_script(json_to_transform_to_python)
        with open(script_path_file, 'rb') as script_file:
            response = HttpResponse(script_file, content_type='application/x-python-code')
            response['Content-Disposition'] = 'attachment; filename="%s"' % script_file.name
        os.remove(script_path_file)
        return response
    else:
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

# is_coherent
# param units: Unit[]
# param yaml_json_object: string[]
# Log on console using logger all errors encountered
def is_coherent(units, yaml_json_object):
    # compare unit names
    is_coherent = True
    for unit in units:
        if not unit.class_name in [x['class_name'] for x in yaml_json_object['classes']]:
            logger.error('Bad class name')
            return False
    if len(units) != len([x['class_name'] for x in yaml_json_object['classes']]):
        logger.error('Bad number of units ' + str(len(units)) + ' units againts ' + str(len([x['class_name'] for x in yaml_json_object['classes']])) + ' yaml units')
        return False
    for unit_class in yaml_json_object['classes']:
        # Get the right unit from units associated
        unit = None
        unit_found = False
        for unit in units:
            if unit.class_name == unit_class['class_name']:
                current_unit = unit
                unit_found = True
                break
        # If the unit is not found, log error and break for loop
        if not unit_found:
            logger.error(unit_class['class_name'] + 'is not in units')
            is_coherent = False
            break
        if 'displayable' in unit_class:
            if unit_class['displayable'] != unit.displayable:
                logger.error(unit_class['class_name'] + 'has bad displayable value')
                is_coherent = False
        else:
            if unit.displayable != True:
                logger.error(unit_class['class_name'] + 'has bad default displayable value')
        if 'parameters' in unit_class:
            if 'parameters_extended' in unit_class and 'extends' in unit_class:
                if len(unit.parameters) != len(unit_class['parameters']) + len(unit_class['parameters_extended']):
                    is_coherent = False
                    logger.error(unit_class['class_name'] + ' has invalid parameters number : ' + str(len(unit.parameters)) + ' unit parameters againts ' + str(len(unit_class['parameters'])) + ' yaml unit parameters plus ' + str(len(unit_class['parameters_extended'])) + ' yaml extended unit parameters')
            else:
                if len(unit.parameters) != len(unit_class['parameters']):
                    is_coherent = False
                    logger.error(unit_class['class_name'] + ' has invalid parameters number : ' + str(len(unit.parameters)) + ' unit parameters againts ' + str(len(unit_class['parameters'])) + ' yaml unit parameters')
        else:
            if 'parameters_extended' in unit_class and 'extends' in unit_class:
                if len(unit.parameters) != len(unit_class['parameters_extended']):
                    is_coherent = False
                    logger.error(unit_class['class_name'] + ' has invalid parameters number : ' + str(len(unit.parameters)) + ' unit parameters againts ' + str(len(unit_class['parameters_extended'])) + ' yaml extended unit parameters')
        if not is_coherent:
            return False
    return True

# extends_in_classe
# param extends_array: string[] = a list of class names
# param units: Unit[] = array of unit
# return True if all class names of extends_array are in units
def extends_in_classe(extends_array, units):
    for extend in extends_array:
        if not extend in [x.name for x in units]:
            return False
    return True


def get_classes(path):
    classes = []
    set_classes_from_pkg(classes, omegalpes.energy.units.__path__, 'omegalpes.energy.units')
    classes = unique_classes(classes)
    energyUnit = None
    for classe in classes:
        classe.set_functions()
        classe.set_parameters()
        # EnergyUnit is considered as the parent classe, We don't consider OptUnit etc ...
        if classe.name == 'EnergyUnit':
            EnergyUnit = classe
        if classe.name != 'EnergyUnit':
            classe.set_extends()
    # Keep only classes who herits from EnergyUnit
    classes = filter_classes_by_extends(classes, EnergyUnit)

    for classe in classes:
        classe.set_extended_classes(classes)
    return classes

# validate_structure
# param: file_path: string = the path of a file to check
# return true if the file in the path file_path respects the schema
def validate_structure(file_path):
    validators = DefaultValidators.copy()  # This is a dictionary
    validators[Default.tag] = Default
    schema = yamale.make_schema('api/config/schema/schema.yaml', validators=validators)
    data = yamale.make_data(file_path)
    yamale.validate(schema, data)


# filter_classes_by_extends
# param classes: Classe[] = the classes found in omegalpes
# param parent: Classe = The parent
# return classes: Classe[] which herits from parent
def filter_classes_by_extends(classes, parent):
    classes_to_keep = [parent]
    is_new_added_classe = True
    while is_new_added_classe:
        is_new_added_classe = False
        for classe in classes:
            if not classe in classes_to_keep:
                if extends_in_classe(classe.extends, classes_to_keep):
                    classes_to_keep.append(classe)
                    is_new_added_classe = True
    return classes_to_keep

# get_error_from
# param yaml_json_object: string[] = yaml file converts in array
# param classes: Classe[] = array of classes
# return for each part of the yaml the errors encountered in errors: string[]
def get_error_from(yaml_json_object, classes):
    errors = []
    for yml_classe in yaml_json_object['classes']:
        if not yml_classe['class_name'] in [x.name for x in classes]:
            levenshtein_array = unique_array(
                get_levenshtein_string_array(yml_classe['class_name'], [x.name for x in classes],
                                             levenshtein_max_distance))
            if len(levenshtein_array) > 0:
                errors.append(yml_classe['class_name'] + ' is not a class of omegalpes. Did you mean : ' + ' or '.join(
                    levenshtein_array) + ' ?')
            else:
                errors.append(yml_classe[
                                  'class_name'] + ' is not a class of omegalpes. Please see the possible classes allowed : ' + ', '.join(
                    [x.name for x in classes]))
            break
        associated_classe = None
        for classe in classes:
            if classe.name == yml_classe['class_name']:
                associated_classe = classe

        if 'parameters' in yml_classe:
            for yml_parameter in yml_classe['parameters']:
                if not associated_classe.has_parameter(yml_parameter['name']):
                    levenshtein_array = unique_array(get_levenshtein_string_array(yml_parameter['name'],
                                                                                  [x.name for x in
                                                                                   associated_classe.parameters],
                                                                                  levenshtein_max_distance))
                    if len(levenshtein_array) > 0:
                        errors.append('The parameter ' + yml_parameter['name'] + ' is not in ' + associated_classe.name + '. Did you mean ' + ' or '.join(
                            levenshtein_array) + ' ?')
                    else:
                        errors.append('The parameter ' + yml_parameter[
                            'name'] + ' is not in ' + associated_classe.name + '. There are the allowed parameters : ' + ', '.join(
                            [x.name for x in associated_classe.parameters]))
                if 'parameters_extended' in yml_classe:
                    if yml_parameter['name'] in yml_classe['parameters_extended']:
                        if associated_classe.has_extended_parameter(yml_parameter['name']):
                            errors.append('The parameter ' + yml_parameter['name'] + ' of ' + associated_classe.name + ' should be in parameters_extended')
        if 'parameters_extended' in yml_classe:
            for yml_parameter in yml_classe['parameters_extended']:
                if not associated_classe.has_extended_parameter(yml_parameter):
                    for classe in associated_classe.extended_classes:
                        levenshtein_array = unique_array(get_levenshtein_string_array(yml_parameter, [x.name for x in
                                                                                                      associated_classe.get_extended_parameters()],
                                                                                      levenshtein_max_distance))
                        if associated_classe.has_parameter(yml_parameter):
                            errors.append(
                                'The extended parameter ' + yml_parameter + ' of class ' + associated_classe.name + ' should be in parameters.')
                            break
                        if len(levenshtein_array) > 0:
                            errors.append(
                                'The extended parameter ' + yml_parameter + ' is not allowed in class ' + associated_classe.name + ' which herits from ' + classe.name + '. Did you mean ' + ' or '.join(
                                    levenshtein_array) + ' ?')
                        else:
                            errors.append(
                                'The extended parameter ' + yml_parameter + ' is not allowed in class ' + associated_classe.name + ' which herits from ' + classe.name + ' and can have as extended parameters : ' + ', '.join(
                                    [x.name for x in classe.parameters]))
                if not associated_classe.has_parameter(yml_parameter):
                    for classe in associated_classe.extended_classes:
                        levenshtein_array = unique_array(get_levenshtein_string_array(yml_parameter,[x.name for x in associated_classe.parameters], levenshtein_max_distance))
                        if len(levenshtein_array) > 0:
                            errors.append('The parameter ' + yml_parameter + ' in parameter_extends is not in ' + associated_classe.name + '. Did you mean ' + ' or '.join(levenshtein_array) + ' ?')
                        else:
                            errors.append('The parameter ' + yml_parameter + ' in parameter_extends is not in ' + associated_classe.name + '. There are the allowed parameters : ' + ', '.join([x.name for x in associated_classe.parameters]))
        if 'objectives' in yml_classe:
            for yml_objective in yml_classe['objectives']:
                if not associated_classe.has_objective(yml_objective['name']):
                    levenshtein_array = unique_array(get_levenshtein_string_array(yml_objective['name'],
                                                                                  [x.name for x in
                                                                                   associated_classe.functions],
                                                                                  levenshtein_max_distance))
                    if len(levenshtein_array) > 0:
                        errors.append('The objective ' + yml_objective[
                            'name'] + ' is not in class ' + associated_classe.name + '. Did you mean : ' + ' or '.join(
                            levenshtein_array))
                    else:
                        errors.append('The objective ' + yml_objective[
                            'name'] + ' is not in class ' + associated_classe.name + '. There are the existing functions : ' + ', '.join(
                            associated_classe.get_non_underscored_functions()))
        if 'extends' in yml_classe:
            for yml_extend in yml_classe['extends']:
                if not associated_classe.has_extend(yml_extend):
                    levenshtein_array = unique_array(
                        get_levenshtein_string_array(yml_extend, associated_classe.extends, levenshtein_max_distance))
                    if len(levenshtein_array) > 0:
                        errors.append(
                            'The class ' + yml_extend + ' cannot be extended by ' + associated_classe.name + '. Did you mean ' + ' or '.join(
                                levenshtein_array) + ' ?')
                    else:
                        errors.append(
                            'The class ' + yml_extend + ' cannot be extended by ' + associated_classe.name + '. The possible extended classes are ' + ', '.join(
                                [x for x in classe.extends]))
        if 'disjoint_objectives' in yml_classe:
            for yml_disjoint in yml_classe['disjoint_objectives']:
                for objective in yml_disjoint['objectives']:
                    if not associated_classe.has_objective(objective):
                        levenshtein_array = unique_array(
                            get_levenshtein_string_array(objective, associated_classe.get_non_underscored_functions(),
                                                         levenshtein_max_distance))
                        if len(levenshtein_array) > 0:
                            errors.append(
                                'The disjoint objective ' + objective + ' is not in class ' + associated_classe.name + '. Did you mean ' + ' or '.join(
                                    levenshtein_array) + ' ?')
                        else:
                            errors.append(
                                'The disjoint objective ' + objective + ' is not in class ' + associated_classe.name + '. There are the existing functions : ' + ', '.join(
                                    associated_classe.get_non_underscored_functions()))
        if 'hide_parent_objectives' in yml_classe:
            for objective in yml_classe['hide_parent_objectives']:
                if not associated_classe.has_extended_objective(objective):
                    for classe in associated_classe.extended_classes:
                        levenshtein_array = get_levenshtein_string_array(objective,
                                                                         associated_classe.get_extended_non_underscored_functions(),
                                                                         levenshtein_max_distance)
                        if associated_classe.has_objective(objective):
                            errors.append(
                                'The objective ' + objective + ' of class ' + associated_classe.name + ' should be in objectives.')
                            break
                        if len(levenshtein_array) > 0:
                            errors.append(
                                'The objective ' + objective + ' is not allowed in ' + associated_classe.name + '. Did you mean : ' + ' or '.join(
                                    levenshtein_array) + ' ?')
                        else:
                            errors.append(
                                'The objective ' + objective + ' is not allowed in ' + associated_classe.name + ' and can have as extended functions : ' + ', '.join(
                                    associated_classe.get_extended_non_underscored_functions()))
        if 'constraints' in yml_classe:
            for constraint in yml_classe['constraints']:
                if associated_classe.has_constraint(constraint['name']):
                    if 'parameters' in constraint:
                        for parameter in constraint['parameters']:
                            if not associated_classe.has_parameter_in_constraint(constraint['name'], parameter['name']):
                                levenshtein_array = get_levenshtein_string_array(parameter['name'], [x.name for x in
                                                                                                     associated_classe.get_constraint_parameters(
                                                                                                         constraint[
                                                                                                             'name'])],
                                                                                 levenshtein_max_distance)
                                if len(levenshtein_array) > 0:
                                    errors.append('The constraint ' + constraint[
                                        'name'] + ' in class ' + associated_classe.name + ' has invalid parameter ' +
                                                  parameter['name'] + '. Did you mean ' + ' or '.join(
                                        levenshtein_array) + ' ?')
                                else:
                                    errors.append('The constraint ' + constraint[
                                        'name'] + ' in class ' + associated_classe.name + ' has invalid parameter ' +
                                                  parameter['name'] + '. There are the existing parameters of ' +
                                                  constraint['name'] + ' : ' + ', '.join(
                                        associated_classe.get_parameters_name_of_constraint(constraint['name'])))
                else:
                    levenshtein_array = get_levenshtein_string_array(constraint['name'],
                                                                     associated_classe.get_non_underscored_functions(),
                                                                     levenshtein_max_distance)
                    if len(levenshtein_array) > 0:
                        errors.append('The constraint ' + constraint[
                            'name'] + ' is not in class ' + associated_classe.name + '. Did you mean : ' + ', '.join(
                            levenshtein_array) + ' ?')
                    else:
                        errors.append('The constraint ' + constraint[
                            'name'] + ' is not in class ' + associated_classe.name + '. There are the existing functions : ' + ', '.join(
                            associated_classe.get_non_underscored_functions()))

    return errors

# unique_classes
# param classes: Classe[] = array of classes
# return an array of unique classes, without repetitions
def unique_classes(classes):
    classes_unique = []
    for classe in classes:
        if not classe.name in [x.name for x in classes_unique]:
            classes_unique.append(classe)
    return classes_unique

# set_classes_from_pkg
# param classes: Classe[] = array of classes
# param path: string[] = array of path
# param module_path: string = the path of the module from where we get classes
def set_classes_from_pkg(classes, path:[], module_path):
    for importer, modname, ispkg in pkgutil.iter_modules(path):
        if ispkg:
            pathObject = Path(path[0])
            set_classes_from_pkg(classes, [pathObject / modname], module_path + '.' + modname)
        else:
            mod = importlib.import_module(module_path + '.' + modname)
            names = getattr(mod, '__all__', [n for n in dir(mod) if not n.startswith('_')])
            g = globals()
            for name in names:
                klass = get_class_by_name_in_package(module_path + '.' + modname + '.' + name)
                try:
                    source_code = inspect.getsource(klass).replace('\n','')
                    source_with_single_spaces = re.sub(r"\s{2,}", " ", source_code)
                    classes.append(Classe(name, module_path + '.' + modname, source_with_single_spaces))
                except TypeError:
                    pass
# get_class_by_name_in_package
# param path: String = the path of the classe to get
# return the classe imported using the path
def get_class_by_name_in_package(path):
    components = path.split('.')
    mod = __import__(components[0])
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod

# get_levenshtein_string_array
# param word_to_use: string
# param array_of_words: string[] = array of words
# param distance_max: int = distance max for levenshtein process
# return array_of_words_to_send: string[] which is composed by words of array_of_words with words that have a levenshtein distance with word_to_use less than distance_max
def get_levenshtein_string_array(word_to_use, array_of_words, distance_max):
    array_of_words_to_send = []
    for word in array_of_words:
        if Levenshtein.distance(word_to_use, word) <= distance_max:
            array_of_words_to_send.append(word)
    return array_of_words_to_send


# Check the logic of the yaml. Check extend field.
def check_logic_validity(yaml_json_object, classes):
    return True


# get_units_from_yaml
# param yaml_json_object: string[] = yaml file converts in array
# return units: Unit[] using yaml_json_object which contain the configuration
def get_units_from_yaml(yaml_json_object):
    units = []
    i = 0
    unit_classes = yaml_json_object['classes']
    unit_class = unit_classes[i]
    energies = [{'energy': x['energy'], 'color': x['color']} for x in yaml_json_object['model']['energies']]
    original_classes_name_len = len(unit_classes)
    while len(units) < original_classes_name_len:
        parameters = []
        objectives = []
        disjoint_objectives = []
        extends = []
        types = []
        constraints = []
        # Default label is class_name if not provided
        unit_class['label'] = unit_class.get('label', unit_class['class_name'])
        # Default displayable is True if not provided
        unit_class['displayable'] = unit_class.get('displayable', True)
        if 'extends' in unit_class:
            extends = unit_class['extends']
        if 'extends' not in unit_class or is_extends_class_in_units(units, extends):
            # All fields without inheritance
            if 'parameters' in unit_class:
                for parameter in unit_class['parameters']:
                    # Fill constraints and parameters together. The signification is very close
                    # if 'default' in parameter:
                    parameters.append(
                        Parameter(label=parameter['label'], name=parameter['name'], type=parameter['type'],
                                  from_class=unit_class['class_name'],
                                  default=parameter.get('default', ''), tooltip=parameter.get('tooltip')))
            # Fill constraints ( its write in parameters field because it's barely the same )
            if 'objectives' in unit_class:
                for objective in unit_class['objectives']:
                    objectives.append(
                        Objective(objective['label'], objective['name'], from_class=unit_class['class_name'], tooltip=objective.get('tooltip')))
            if 'constraints' in unit_class:
                for constraint in unit_class['constraints']:
                    constraint_parameters = []
                    if 'parameters' in constraint:
                        for constraint_parameter in constraint['parameters']:
                            constraint_parameters.append(
                            Parameter(label=constraint_parameter['label'], name=constraint_parameter['name'], type=constraint_parameter['type'], from_class=unit_class['class_name'], default=constraint_parameter.get('default', ''), tooltip=constraint_parameter.get('tooltip')))
                    constraints.append(Constraint(label=constraint['label'], name=constraint['name'], parameters=constraint_parameters, from_class=unit_class['class_name'], tooltip=constraint.get('tooltip')))
            if 'type' in unit_class:
                types.append(unit_class['type'])
            # Apply inheritance
            for class_name in extends:
                for unit in units:
                    if unit.class_name == class_name:
                        # This line allows you to know from which parent each parameters extends ( not needed for now )
                        # parameters = parameters + unit.parameters_copy()
                        objectives = objectives + unit.objectives
                        types = types + unit.types
                        disjoint_objectives = disjoint_objectives + unit.disjoint_objectives
                        constraints = constraints + unit.constraints
                        # All parameters of parents are stored in parameters
                        parameters = parameters + unit.get_all_extended_parameters() + unit.parameters
            objectives = unique_array(objectives)
            types = unique_array(types)
            disjoint_objectives = unique_array(disjoint_objectives)
            constraints = unique_array(constraints)
            if 'parameters_extended' in unit_class:
                # All parameters of parents are currently in parameters. I will filter them
                parameters_to_keep = [x for x in parameters if x.from_class == unit_class['class_name']]
                # Keep only parameters of parents that are in parameters_extended
                for extended_parameter_name in unit_class['parameters_extended']:
                    for parameter in [x for x in parameters if x.from_class != unit_class['class_name']]:
                        if extended_parameter_name == parameter.name:
                            parameters_to_keep.append(parameter)
                            break
                parameters = parameters_to_keep

                # Keep in parameters only parameters that are in parameter_extended
            parameters = unique_parameters(parameters)
            if 'hide_parent_objectives' in unit_class:
                for parent_objective in unit_class['hide_parent_objectives']:
                    for objective in objectives:
                        if objective.name == parent_objective:
                            objectives = [objective for objective in objectives if objective.name != parent_objective]
            if 'disjoint_objectives' in unit_class:
                for disjoint_objective in unit_class['disjoint_objectives']:
                    objectives_to_add = []
                    for objective in objectives:
                        if objective.name in disjoint_objective['objectives']:
                            # Need to copy the object because it will be deleted during repetion removals
                            objectives_to_add.append(copy.deepcopy(objective))
                    disjoint_objectives.append(
                        DisjointObjective(disjoint_objective['label'], objectives_to_add, unit_class['class_name'],
                                          tooltip=disjoint_objective.get('tooltip')))
                    # remove repetitons between disjoint_objectives and objectives.
                    for objective_to_remove in objectives_to_add:
                        for objective in objectives:
                            if objective_to_remove.name == objective.name:
                                del objectives[objectives.index(objective)]
            unit = Unit(unit_class['label'], unit_class['class_name'], unit_class['displayable'], types, parameters,
                 constraints,
                 objectives, disjoint_objectives, extends, energies)
            unit.set_extended_units(units)
            units.append(unit)
            del unit_classes[i]
        for unit in units:
            unit.set_all_extends_in_order()
        if len(unit_classes) > 0:
            i = (i + 1) % len(unit_classes)
            unit_class = unit_classes[i]
    return units
    # json.dumps([x.asdict() for x in units])


# build_json_response
# param units: Unit[]
# param yaml_json_object: string[] = yaml file converts to array
# return a dict

def build_json_response(units, yaml_json_object):
    energies = [{'energy': x['energy'], 'color': x['color']} for x in yaml_json_object['model']['energies']]
    units.append(Unit('Node', 'EnergyNode', True, ['Node'], [Parameter('Name', 'name', 'str', 'Node'), Parameter( 'Energy type','energy_type', 'str', 'Node', default=None)], [], [], [], [], energies))
    model_parameters = []
    if 'model' in yaml_json_object:
        if 'parameters' in yaml_json_object['model']:
            for parameter in yaml_json_object['model']['parameters']:
                model_parameters.append(Parameter(parameter['label'], parameter['name'], parameter['type'], '', parameter.get('default', ''), parameter.get('tooltip')))
    return {
        'version': yaml_json_object['omegalpes_version'],
        'model': {
            'parameters': [x.as_dict() for x in model_parameters],
            'energies': [{'energy': x['energy'], 'color': x['color']} for x in yaml_json_object['model']['energies']]
        },
        'unit_groups': [
            {
                'group_name': 'Production units',
                'flow_direction': 'output',
                'units': [x.as_dict() for x in units if 'Production' in x.types if x.displayable]
            },
            {
                'group_name': 'Consumption units',
                'flow_direction': 'input',
                'units': [x.as_dict() for x in units if 'Consumption' in x.types if x.displayable]
            },
            {
                'group_name': 'Storage units',
                'flow_direction': 'input',
                'units': [x.as_dict() for x in units if 'Storage' in x.types if x.displayable]
            },
            {
                'group_name': 'Node',
                'flow_direction': 'both',
                'units': [x.as_dict() for x in units if 'Node' in x.types if x.displayable]
            }
        ]
    }


# get_unit_by_class_name
# param units: Unit[]
# param classname: string
# return unit: Unit from units that have the same class name
def get_unit_by_class_name(units, classname):
    for unit in units:
        if unit.class_name == classname:
            return unit
    return None


# is_class_in_units
# param units: Unit[]
# param classname: string
# return True if classname is a name of a classe in units, else return False
def is_class_in_units(units, classname):
    for unit in units:
        if unit.class_name == classname:
            return True
    return False


# is_extends_class_in_units
# param units: Unit[]
# param classes_name: string[] = array of classe names
# return True of all classe names of classes_name are in units, else return False
def is_extends_class_in_units(units, classes_name):
    for class_name in classes_name:
        if not is_class_in_units(units, class_name):
            return False
    return True
