import re
from .unit import Parameter

# class Classe
class Classe:
    def __init__(self, name, module_path, source_code):
        self.name = name
        self.module_path = module_path
        self.source_code = source_code
        # String: Classe that extends current
        self.extends = []
        # Classe: classe that extends current
        self.extended_classes = []
        # Parameter:
        self.parameters = []
        # String: all functions of current
        self.functions = []

    # set_parameters is setter of parameters
    def set_parameters(self):
        self.parameters = self.__get_parameters_from_source_code()

    # set_functions is setter of functions
    def set_functions(self):
        self.functions = self.__get_functions_from_source_code()

    # set_extends is setter of extends
    def set_extends(self):
        self.extends = self.__get_extends_classes_from_source_code()

    # set_extended_classes is setter of extended classes
    def set_extended_classes(self, classes):
        self.__set_extended_classes_from_existing_classes(classes)

    # has_parameter
    # param parameter: string
    # return True if parameter is in self.parameters.name, else return False
    def has_parameter(self, parameter):
        return parameter in [x.name for x in self.parameters]

    # has_extended_parameter
    # param parameter: string
    # return True if a parent has parameter in his parameters.name, else return False
    def has_extended_parameter(self,parameter):
        has_extended_parameter = False
        for extended_classe in self.extended_classes:
            has_extended_parameter = extended_classe.has_parameter(parameter) or extended_classe.has_extended_parameter(parameter)
            if has_extended_parameter:
                return True
        return False

    # get_extended_parameters
    # return extended_params: Parameter[], all parameters of parents
    def get_extended_parameters(self):
        extended_params = []
        for extended_classe in self.extended_classes:
            extended_params = extended_params + extended_classe.parameters + extended_classe.get_extended_parameters()
        return extended_params

    # get_extended_funtions
    # return extended_functions: Function[], all functions of parents
    def get_extended_funtions(self):
        extended_functions = []
        for extended_classe in self.extended_classes:
            extended_functions = extended_functions + extended_classe.functions + extended_classe.get_extended_funtions()
        return extended_functions

    # has_function_name
    # param function_name: string
    # return True if function_name is in self.functions.name, else return False
    def has_function_name(self, function_name):
        return function_name in [x.name for x in self.functions]

    # __get_function_by_name
    # param function_name: string
    # return function: Function using function_name
    # raise AttributeError
    def __get_function_by_name(self, function_name):
        try:
            return [x for x in self.functions if x.name == function_name][0]
        except AttributeError:
            raise AttributeError('The function name ' + function_name + ' does not exist in class')

    # has_parameter_in_function
    # param function_name: string
    # param parameter_string: string
    # return True if function has parameter parameter_string
    def has_parameter_in_function(self, function_name, parameter_string):
        function = self.__get_function_by_name(function_name)
        return function.has_parameter(parameter_string)

    # get_non_underscored_functions
    # return all functions that not begin by _
    def get_non_underscored_functions(self):
        return [x.name for x in self.functions if not x.name.startswith('_')]

    # get_extended_non_underscored_functions
    # return all functions of parents that not begin by _
    def get_extended_non_underscored_functions(self):
        return [x.name for x in self.get_extended_funtions() if not x.name.startswith('_')]

    # get_parameters_name_of_function
    # param function_name: string
    # return parameters names of the function that have function_name
    def get_parameters_name_of_function(self, function_name):
        try:
            function = self.__get_function_by_name(function_name)
            return [x.name for x in function.parameters]
        except AttributeError:
            return []

    # get_parameters_name_of_constraint
    # param constraint_name: string
    # return parameters name of constraint that have constraint name
    def get_parameters_name_of_constraint(self, constraint_name):
        return self.get_parameters_name_of_function(constraint_name)

    # has_parameter_in_constraint
    # param constraint_name: string
    # param parameters_string: string[]
    # return True if parameters_string are in constraint parameters name
    def has_parameter_in_constraint(self, constraint_name, parameters_string):
        return self.has_parameter_in_function(constraint_name, parameters_string)

    # has_objective
    # param objective_name: string
    # return True class has objective objective_name
    def has_objective(self, objective_name):
        return self.has_function_name(objective_name)

    # has_constraint
    # param constraint_name: string
    # return True if class has constraint with constraint_name
    def has_constraint(self, constraint_name):
        return self.has_function_name(constraint_name)

    # get_constraint_parameters
    # param constraint_name: string
    # return parameters: Parameter[] of constraint with constraint name
    def get_constraint_parameters(self, constraint_name):
        return self.__get_function_by_name(constraint_name).parameters

    # has_extended_objective
    # param objective_name: string
    # return True if parent class has objective with objective_name
    def has_extended_objective(self, objective_name):
        return self.has_extended_function_name(objective_name)

    # has_extended_function_name
    # param function_name: string
    # return True if class has function with function_name, else return False
    def has_extended_function_name(self, function_name):
        return function_name in [x.name for x in self.get_extended_funtions()]

    # has_extend
    # param extend
    # return True if extend is in self.extends, else return False
    def has_extend(self, extend):
        return extend in self.extends

    # __get_extends_classes_from_source_code
    # return extends classes from source code of the classe
    def __get_extends_classes_from_source_code(self):
        extends = []
        try:
            extends_string = re.search('class\s[^(]*\(([^)]*)\)', self.source_code).group(1)
            extends_string_without_spaces = re.sub(r"\s", "", extends_string)
            for extend_string in extends_string_without_spaces.split(','):
                extends.append(extend_string)
        except AttributeError:
            pass
        return extends

    # __get_parameters_from_source_code
    # return parameters: Parameter[] of the class. The parameters returned are in the constructor of the classe in source code
    def __get_parameters_from_source_code(self):
        parameters = []
        parameters_string = ""
        try:
            parameters_string = re.search('def\s__init__\s?\(\s?self\s?,\s?time\s?,([^)]*)', self.source_code).group(1)
        except AttributeError:
            pass
        parameters_string_without_spaces = re.sub(r"\s", "", parameters_string)
        for parameter_string in parameters_string_without_spaces.split(','):
            default = ''
            type = ''
            name = ''
            p = re.compile(r'(?P<name>[^:=]*)(:(?P<type>[^:=]*))?(=(?P<default>[^:=]*))?')
            m = p.search(parameter_string)
            name = m.group('name')
            type = m.group('type')
            default = m.group('default')
            if default == None:
                if type == None:
                    parameters.append(Parameter('', name, '', self.name))
                else:
                    parameters.append(Parameter('', name, type, self.name))
            else:
                if type == None:
                    parameters.append(Parameter('', name, '', self.name, default=default))
                else:
                    parameters.append(Parameter('', name, type, self.name, default=default))
        return parameters

    # __get_functions_from_source_code
    # return functions: Function[] of the class
    def __get_functions_from_source_code(self):
        functions_string = []
        functions = []
        pattern = re.compile(r'def\s([^(\s]*)')

        for function in re.findall(pattern, self.source_code):
            functions_string.append(function)

        for function_name in functions_string:
            parameters = []

            try:
                parameters_string = re.search('def\s' + function_name + '\s?\(\s?self\s?,([^)]*)', self.source_code).group(1)
                parameters_string_without_spaces = re.sub(r"\s", "", parameters_string)
                # This line is needeed for the particular case operating_time_range=[[int,int]]
                parameters_string_without_unwanted_commas = re.sub(r"\s?=\s?\[[^\]]+\]+", "", parameters_string_without_spaces)
                for parameter_string in parameters_string_without_unwanted_commas.split(','):
                    # Add the case for "attr: type = default"
                    if '=' in parameter_string:
                        (name, default) = tuple(parameter_string.split('='))
                        parameters.append(Parameter('', name, '', self.name, default))
                    else:
                        parameters.append(Parameter('', parameter_string, '', self.name))
            except AttributeError:
                pass
            functions.append(Function(function_name, parameters))
        return functions

    # __set_extended_classes_from_existing_classes
    # param classes: Classe[] = all classes
    # set self.extended_classes with the classes objects from param classes
    def __set_extended_classes_from_existing_classes(self, classes):
        for classe in classes:
            if classe.name in self.extends:
                self.extended_classes.append(classe)

# class Function
class Function:
    def __init__(self, name, parameters):
        self.name = name
        self.parameters = parameters

    # has_parameter
    # param parameter_name: string
    # return True if parameter_name s in setlf.parameters, else return False
    def has_parameter(self, parameter_name):
        return parameter_name in [x.name for x in self.parameters]
