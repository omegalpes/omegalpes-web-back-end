import copy

# BaseUnit: Interface
class BaseUnit:
    # as_dict
    # return a dict
    # raise exception NotImplementedError if not implemented
    def as_dict(self):
        raise NotImplementedError("You have to implement as_dict function")

# Unit
class Unit(BaseUnit):

    # __init__
    # param label: stirng
    # param class_name: string
    # param displayable: bool
    # param types: string[]
    # param parameters: Parameter[]
    # param constraints: Constraint[]
    # param objectives: Objective[]
    # param disjoint_objectives: DisjointObjective[]
    # param extends: string[]
    # param energies: dict = example :
    # {
    #   {
    #       name: string,
    #       color: string ( hexadecimal color )
    #   }
    # }
    def __init__(self, label, class_name, displayable, types, parameters, constraints, objectives,
                 disjoint_objectives, extends, energies):
        if self._check_parameters_types(parameters) and self._check_objectives_types(objectives):
            self.label = label
            self.class_name = class_name
            self.displayable = displayable
            self.types = types
            self.subtype = 'unspecified'
            if 'Fixed' in self.types:
                self.subtype = 'fixed'
            if 'Variable' in self.types:
                self.subtype = 'variable'
            if 'Variable' in self.types and 'Storage' in self.types:
                self.subtype = 'storage'
            if 'Node' in self.types:
                self.subtype = 'node'
            self.parameters = parameters
            self.constraints = constraints
            self.objectives = objectives
            self.disjoint_objectives = disjoint_objectives
            self.energies = energies
            self.extends = extends
            self.extended_units = []
            self.all_extends_in_order = []
        else:
            raise ValueError("Please verify the types of parameters and objectives")
    # _check_parameters_types
    # param parameters: any[]
    # return true if parameters is type Parameter[], else return False
    def _check_parameters_types(self, parameters):
        return all(isinstance(n, Parameter) for n in parameters)

    # set_extended_units
    # param units: Unit[]
    def set_extended_units(self, units):
        for unit in units:
            for extend in self.extends:
                if unit.class_name == extend:
                    self.extended_units.append(unit)

    # set_all_extends_in_order
    # Fill all_extends in order. The order is from younger to older. For example, EnergyUnit is alwways at the end. Current class is always in beginning
    def set_all_extends_in_order(self):
        extended_couples = [(self.class_name, 0)]
        for extend in self.extends:
            extended_couples = extended_couples + [(extend, 1)]
        for extended_unit in self.extended_units:
            extended_couples =  extended_couples + extended_unit.__get_extended_couple(2)
        extended_couples = list(set(extended_couples))
        extended_couples = sorted(extended_couples, key = lambda x : x[1], reverse=False)
        self.all_extends_in_order = [x[0] for x in extended_couples]

    # get_all_extended_parameters
    # return all parameters of parent
    def get_all_extended_parameters(self):
        extended_parameters = []
        for unit in self.extended_units:
            extended_parameters = extended_parameters + unit.parameters
            extended_parameters = extended_parameters + unit.get_all_extended_parameters()
        return extended_parameters

    # __get_extended_couple
    # param step
    # return array of extended couple
    # Recursive function. Return array like this [(extended_classe, step), (self.extended_classe.extends, step + 1)]
    def __get_extended_couple(self, step):
        extended_couple = []
        for extend in self.extends:
            extended_couple = extended_couple + [(extend, step)]
        for extended_unit in self.extended_units:
            extended_couple = extended_couple + extended_unit.__get_extended_couple(step+1)
        return extended_couple

    # _check_objectives_types
    # param objectives: any[]
    # return true if objectives is type Objective[], else return False
    def _check_objectives_types(self, objectives):
        return all(isinstance(n, Objective) for n in objectives)

    # parameters_copy
    # return copy of parameters
    def parameters_copy(self):
        parameters_copy = []
        for parameter in self.parameters:
            parameters_copy.append(copy.copy(parameter))
        return parameters_copy

    # __has_constraints
    # param extend: string
    # return True if the self.constraints have a constraint from class extend
    def __has_constraints(self, extend):
        return extend in [x.from_class for x in self.constraints]

    # __has_objectives
    # param extend: string
    # return True if the self.objectives have a objective from class extend
    def __has_objectives(self, extend):
        return extend in [x.from_class for x in self.objectives]

    # __has_disjoint_objectives
    # param extend: string
    # return True if the self.disjoint_objectives have a disjoint_objective from class extend
    def __has_disjoint_objectives(self, extend):
        return extend in [x.from_class for x in self.disjoint_objectives]

    # __construct_objectives_array
    # return a dict for th objectives array
    def __construct_objectives_array(self):
        response = []
        # Mettre self.extends ici. Ne pas ajouter un append si pas d'objectifs
        from_classes_array = self.all_extends_in_order
        for classe in from_classes_array:
            if len([x for x in self.objectives if x.from_class == classe]) != 0 or len([x.as_dict() for x in self.disjoint_objectives if x.from_class == classe]) != 0:
                response.append({
                    'type': classe,
                    'objectives': [x.as_dict() for x in self.objectives if x.from_class == classe],
                    'disjoint_objectives': [x.as_dict() for x in self.disjoint_objectives if x.from_class == classe]
                })
        return response

    def as_dict(self):
        return {
            'label': self.label,
            'classname': self.class_name,
            'energies': self.energies,
            'types': self.types,
            'subtype': self.subtype,
            'displayable': self.displayable,
            'parameters': [x.as_dict() for x in self.parameters ],
            'constraints_groups': [{'type': e, 'constraints': [x.as_dict() for x in self.constraints if x.from_class == e]} for
                            e in self.all_extends_in_order if self.__has_constraints(e)],
            'objectives_groups': self.__construct_objectives_array()
        }


# Parameter represent a parameter of a function
class Parameter(BaseUnit):
    def __init__(self, label, name, type, from_class, default='', tooltip=None):
        self.tooltip = tooltip
        self.name = name
        self.label = label
        self.type = type
        self.required = default == ''
        self.default = default if not self.required else ''
        self.from_class = from_class

    def as_dict(self):
        return {
            'name': self.name,
            'label': self.label,
            'type': self.type,
            'required': self.required,
            'default': self.default,
            'tooltip': self.tooltip
        }

# Objective
class Objective(BaseUnit):
    def __init__(self, label, name, from_class, tooltip = None):
        self.tooltip = tooltip
        self.name = name
        self.label = label
        self.from_class = from_class

    def as_dict(self):
        return {
            'name': self.name,
            'label': self.label,
            'tooltip': self.tooltip
        }

# DisjointObjective
class DisjointObjective(BaseUnit):

    # __init__
    # param label: string
    # param objectives: Objective[]
    # param from_class: string
    # param tooltip: string
    def __init__(self, label, objectives, from_class, tooltip=None):
        self.tooltip = tooltip
        self.label = label
        self.objectives = objectives
        self.from_class = from_class

    # get_objectives_len
    # return length of objectives
    def get_objectives_len(self):
        return len(self.objectives)

    def as_dict(self):
        return {
            'label': self.label,
            'objectives': [x.as_dict() for x in self.objectives],
            'tooltip': self.tooltip
        }

# Constraint
class Constraint(BaseUnit):
    def __init__(self, label, name, parameters, from_class, tooltip=None):
        self.tooltip = tooltip
        self.label = label
        self.name = name
        self.parameters = parameters
        self.from_class = from_class

    def as_dict(self):
        return {
            'label': self.label,
            'constraint_name': self.name,
            'parameters': [x.as_dict() for x in self.parameters],
            'tooltip': self.tooltip
        }

# unique_array
# param array: any[] = an array of objects
# return an array of unique array, without repetitions
def unique_array(array):
    array_unique = []
    for el in array:
        if not el in array_unique:
            array_unique.append(el)
    return array_unique

# unique_parameters
# param parameters: Parameter[]
# return unique array of parameters
def unique_parameters(parameters):
    array_unique = []
    for parameter in parameters:
        if not parameter.name in [x.name for x in array_unique]:
            array_unique.append(parameter)
    return array_unique
