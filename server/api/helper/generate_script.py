from datetime import datetime
from string import Template
from pathlib import Path
import time
from zipfile import ZipFile
import json
import os
import autopep8


def to_snake_case(string):
    """ Transforms a string to snake case

    :example:

    >>> not_snake_case = 'Production Unit'
    >>> snake_case = to_snake_case(not_snake_case)
    >>> snake_case
    'production_unit'

    :param string: The string to transform
    :return: String transformed in snake case
    :rtype: string
    """
    return string.lower().replace(' ', '_')


def generate_python_script(json_model):
    headers = generate_headers()
    time_string = generate_time(json_model['modelParametersConfig'])
    units, units_variable_names = generic_generate(json_model['unitsInstance'])
    nodes, nodes_variable_names = generic_generate(json_model['nodesInstance'])
    nodes_connections = generate_nodes_connections(json_model['nodesInstance'], json_model['unitsInstance'])
    units_objectives = generate_units_objectives(json_model['unitsInstance'])
    units_constraints = generate_units_constraints(json_model['unitsInstance'])
    optimization_model = generate_optimization_model(json_model['nodesInstance'])
    return_string = generate_return(units_variable_names, nodes_variable_names)
    results = generate_results(units_variable_names, nodes_variable_names)
    main = generate_main(units_variable_names, nodes_variable_names)
    return assemble_scripts(headers, time_string, units, nodes, nodes_connections, units_objectives, units_constraints,
                            optimization_model, return_string, results, main, json_model)


def generate_headers():
    """Generates the header of the script (shebang, imports).

    Those need to be written manually since there is no dynamic import in the script generation.

    :return: List of the headers as strings
    :rtype: list
    """
    headers = ['#! usr/bin/env python3', '#  -*- coding: utf-8 -*-',
               'from omegalpes.energy.units.consumption_units import *',
               'from omegalpes.energy.units.production_units import *',
               'from omegalpes.energy.energy_nodes import *',
               'from omegalpes.general.time import *',
               'from omegalpes.general.optimisation.model import *',
               'from omegalpes.general.utils.plots import *',
               'from omegalpes.general.utils.output_data import save_energy_flows',
               'from pulp import LpStatus']
    return headers


def generate_main(units_variable_names, nodes_variable_names):
    """Generates the call of the main() function

    Variable names are converted to upper case

    :param units_variable_names: List of the units names
    :param nodes_variable_names: List of the nodes names
    :return: List containing the main() call and print_result() as strings
    :rtype: list
    """
    call_main_strings = []
    call_main_string = 'MODEL, TIME, '
    for unit_variable_name in units_variable_names:
        call_main_string += '%s, ' % (unit_variable_name.upper())
    for node_variable_name in nodes_variable_names:
        call_main_string += '%s, ' % (node_variable_name.upper())
    call_main_string = call_main_string[:-2]
    call_main_string += ' = main()'
    call_main_strings.append(call_main_string)
    call_main_strings.append('print_results()')
    return call_main_strings


def generate_time(model_parameters_config):
    """Generates the TimeUnit

    TimeUnit class is hardcoded since it's not returned by the front-end JSON.
    It needs to be updated manually if it changes in the omegalpes package.

    :param model_parameters_config: Global parameters
    :return: String representing the TimeUnit
    :rtype: str
    """
    time_string = 'time = TimeUnit('
    props = generic_props_generate(model_parameters_config['properties'])
    for prop in props:
        time_string += prop
    time_string += ')'
    return time_string


def generate_nodes_connections(nodes, units):
    """Generates nodes connections with both units and other nodes

    :param nodes: List of nodes
    :param units: List of units
    :return: List containing nodes connections as strings
    :rtype: list
    """
    nodes_connections = []
    for node in nodes:
        # Connection between a node and units
        node_connection_unit = '%s.connect_units(' % to_snake_case(node['variableName'])
        for unit_id in node['linksUnits']:
            for unit in units:
                if unit_id == unit['id']:
                    node_connection_unit += '%s, ' % to_snake_case(unit['variableName'])
        node_connection_unit = node_connection_unit[:-2]
        node_connection_unit += ')'
        nodes_connections.append(node_connection_unit)

        # Connection between a node and another node
        for node_id in node['exportsToNode']:
            for node_bis in nodes:
                if node_id == node_bis['id']:
                    node_connection_node = '%s.export_to_node(%s)' % (
                        to_snake_case(node['variableName']), to_snake_case(node_bis['variableName']))
                    nodes_connections.append(node_connection_node)
    return nodes_connections


def generate_units_objectives(units):
    """Generates units objectives

    :param units: List of units
    :return: List of units objectives as strings
    :rtype: list
    """
    units_objectives = []
    for unit in units:
        for objective in unit['objectives']:
            units_objectives.append('%s.%s()' % (to_snake_case(unit['variableName']), objective['name']))
    return units_objectives


def generate_units_constraints(units):
    """Generates units constraints

    :param units: List of units
    :return: List of units constraints as strings
    :rtype: list
    """
    units_constraints = []
    for unit in units:
        for constraint in unit['constraints']:
            unit_constraint = '%s.%s(' % (to_snake_case(unit['variableName']), constraint['name'])
            props = generic_props_generate(constraint['properties'])
            for prop in props:
                unit_constraint += prop
            unit_constraint += ')'
            units_constraints.append(unit_constraint)
    return units_constraints


def generate_optimization_model(nodes):
    """Generates optimization model and its methods

    OptimisationModel class is hardcoded since it's not returned by the front-end JSON.
    It needs to be updated manually if it changes in the omegalpes package.

    :param nodes: List of nodes
    :return: List containing the optimization model and its methods as strings
    :rtype: list
    """
    optimization_model = ['model = OptimisationModel(time, name=\'optimization_model\')']
    if len(nodes) > 0:
        model_nodes = 'model.add_nodes('
        for node in nodes:
            model_nodes += '%s, ' % to_snake_case(node['variableName'])
        model_nodes = model_nodes[:-2]
        model_nodes += ')'
        optimization_model.append(model_nodes)
    optimization_model.append('model.solve_and_update()')
    return optimization_model


def generate_return(units_variable_names, nodes_variable_names):
    """Generates the parameters to return

    Model and time parameters are hardcoded since they're not returned by the front-end JSON.
    They need to be updated manually if they change in the omegalpes package.

    :param units_variable_names: List of the units names
    :param nodes_variable_names: List of the nodes names
    :return: String containing the parameters to return
    :rtype: str
    """
    return_string = 'return model, time, '
    for unit_variable_name in units_variable_names:
        return_string += '%s, ' % unit_variable_name
    for node_variable_name in nodes_variable_names:
        return_string += '%s, ' % node_variable_name
    return_string = return_string[:-2]
    return return_string


def generate_results(units_variable_names, nodes_variable_names):
    """Generates the methods and functions to display results of the model

    :param units_variable_names: List of the units names
    :param nodes_variable_names: List of the nodes names
    :return: List containing the results methods and functions as strings
    :rtype: list
    """
    results = ['if LpStatus[MODEL.status] == \'Optimal\':']
    for unit_variable_name in units_variable_names:
        results.append(
            '    print(\'%s = {0} kWh.\'.format(%s.e_tot))' % (unit_variable_name, unit_variable_name.upper()))
    for node_variable_unit in nodes_variable_names:
        results.append('    plot_node_energetic_flows(%s)' % node_variable_unit.upper())
    results.append('    plt.show()')
    results.append('elif LpStatus[MODEL.status] == \'Infeasible\':')
    results.append('    print(\'Sorry, the optimisation problem has no feasible solution !\')')
    results.append('elif LpStatus[MODEL.status] == \'Unbounded\':')
    results.append('    print(\'The cost function of the optimisation problem is unbounded !\')')
    results.append('elif LpStatus[MODEL.status] == \'Undefined\':')
    results.append(
        '    print(\"Sorry, a feasible solution has not been found (but may exists). '
        'PuLP does not manage to interpret the solver\'s output,'
        'the infeasibility of the MILP problem may have been'
        'detected during presolve\")')
    return results


def generic_props_generate(props):
    """Generic function to generate 'properties' in general

    :param props: Properties of the component
    :return: List of the properties as strings
    :rtype: list
    """
    props_string = []
    for prop in props:
        if prop['name'] == 'name':
            snake_case_name = prop['value'].lower().replace(' ', '_')
            props_string.append('%s=\'%s\', ' % (prop['name'], snake_case_name))
        elif prop['type'] == 'str' or prop['type'] == 'date':
            props_string.append('%s=\'%s\', ' % (prop['name'], prop['value']))
        elif prop['type'] == 'list':
            props_string.append('%s=[%s], ' % (prop['name'], prop['value']))
        else:
            props_string.append('%s=%s, ' % (prop['name'], prop['value']))
    # Remove last comma and space
    props_string[-1] = props_string[-1][:-2]
    return props_string


def generic_generate(components):
    """Generic function to generate component such as units or nodes

    :param components: List of components (units or nodes at the moment) to generate
    :return: List of components as strings
    """
    components_strings = []
    components_variable_names_strings = []
    for component in components:
        snake_case_variable_name = to_snake_case(component['variableName'])
        component_string = '%s = ' % snake_case_variable_name
        components_variable_names_strings.append('%s' % snake_case_variable_name)
        component_string += '%s(time, ' % component['className']
        props = generic_props_generate(component['properties'])
        for prop in props:
            component_string += prop
        component_string += ')'
        components_strings.append(component_string)
    return components_strings, components_variable_names_strings


def assemble_scripts(headers, time_string, units, nodes, nodes_connections, units_objectives, units_constraints,
                     optimization_model, return_string, results, main, json_model):
    """Creates a zip file containing previously generated parts of the script into a Python file and the JSON model.

    It fills a template file (script_template.txt) with the parts of the script into a Python file.
    It saves the JSON model in a JSON file.
    Zips both files and then deletes them before return the path to the zip file.

    :param headers: Header of the script (shebang, imports)
    :param time_string: TimeUnit
    :param units: Units (EnergyUnits, ConsumptionUnits, ...)
    :param nodes: Node (EnergyNodes)
    :param nodes_connections: Nodes connections with both units and other nodes
    :param units_objectives: Units objectives (methods)
    :param units_constraints: Units constraints (methods)
    :param optimization_model: Optimization model and its methods
    :param return_string: Parameters to return
    :param results: Methods and functions to display results of the model
    :param main: Call of the main() and print_results() functions
    :param json_model: JSON object sent by the front-end
    :return: Path of the zip file
    :rtype: str
    """
    assets_folder = Path('api/assets')
    scripts_folder = Path('scripts')

    time_now = time.time()
    script_file_name = 'script-%s.py' % time_now
    json_file_name = 'script-%s.json' % time_now
    zip_file_name = '%s.zip' % time_now

    template_file_path = assets_folder / 'script_template.txt'
    script_file_path = scripts_folder / script_file_name
    json_file_path = scripts_folder / json_file_name
    zip_file_path = scripts_folder / zip_file_name

    with open(template_file_path, 'r') as tf:
        template_dict = {
            'headers': '\n'.join(headers),
            'time': time_string,
            'units': '\n    '.join(units),
            'nodes': '\n    '.join(nodes),
            'node_connections': '\n    '.join(nodes_connections),
            'unit_objectives': '\n    '.join(units_objectives),
            'unit_constraints': '\n    '.join(units_constraints),
            'optimization_model': '\n    '.join(optimization_model),
            'return': return_string,
            'results': '\n    '.join(results),
            'main_lines': '\n    '.join(main)
        }

        src = Template(tf.read())
        script = src.substitute(template_dict)

    with open(script_file_path, 'w') as python_script:
        script = autopep8.fix_code(script)
        python_script.write(script)

    with open(json_file_path, 'w') as json_file:
        json_file.write(json.dumps(json_model))

    zip_file = ZipFile(zip_file_path, 'w')
    zip_file.write(script_file_path)
    zip_file.write(json_file_path)
    zip_file.close()

    # os.remove(script_file_path)
    os.remove(json_file_path)
    return script_file_path
