from django.apps import AppConfig
from . import checks
from django.core import checks

class ApiConfig(AppConfig):
    name = 'api'
