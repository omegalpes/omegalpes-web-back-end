from django.core.checks import Error, register
from .views import get_classes, validate_structure, get_error_from
import omegalpes.energy.units
from pathlib import Path
import yaml

# yaml_check
# emits an error when django starts if yaml configuration is not valid
@register()
def yaml_check(app_configs, **kwargs):
    errors = []
    errors_yaml = []
    try:
        validate_structure('api/config/config.yaml')
        is_schema_valid = True
    except ValueError as e:
        is_schema_valid = False
        errors.append(
            Error(
                'Error in config/config.yaml. Structure is uncorrect',
                hint=e,
                obj=None,
                id='api.E001',
            )
        )
    if is_schema_valid:
        classes = get_classes('api/config/config.yaml')

        folder = Path('api/config')
        yaml_file = folder / 'config.yaml'
        with open(yaml_file, 'r') as f:
            yaml_json_object = yaml.load(f, Loader=yaml.UnsafeLoader)
            try:
                errors_yaml = get_error_from(yaml_json_object, classes)
                is_grammar_ok = errors_yaml  == []
            except AttributeError:
                is_grammar_ok = False

        if not is_grammar_ok:
            for error in errors_yaml:
                errors.append(
                    Error(
                        'Error in config/config.yaml. A element is uncorrect',
                        hint=error,
                        obj=None,
                        id='api.E002',
                    )
                )

    return errors
