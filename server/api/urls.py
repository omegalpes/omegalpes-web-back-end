from django.urls import path
from .views import config_file, configtest, python_script

urlpatterns = [
    path('api/configfile/', config_file, name='config-file'),
    path('api/pythonscript', python_script, name='python-script'),
    path('api/configtest/', configtest, name='configtest')
]
