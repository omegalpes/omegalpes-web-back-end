set ENVDIR=env
set DJANGO_DIR=server

python -m pip install --upgrade pip
python -m pip install Virtualenv
echo "Create a virtualenv in %ENVDIR%"
mkdir %ENVDIR%
python -m virtualenv %ENVDIR%
cmd /k "%ENVDIR%\Scripts\activate.bat & py -m pip install -r %DJANGO_DIR%\deploy\conf\requirements.txt"