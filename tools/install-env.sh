#!/bin/sh
set -x
ROOT_DIR=$(pwd)/.. # root dir is one level up to django
DJANGO_DIR=$(pwd)
DEPLOY_DIR=$DJANGO_DIR/project/deploy
# Select virtualenv directory
if [ $# -eq 0 ]; then
  # there is no argument, set defaut env
  ENVDIR=env
else
  ENVDIR=$1
fi
# Prerequisites
# run as sudoer
# installs
echo -e "Install ubuntu packages for production environment ? [Y/n]"
read INPUT_STRING
if [ "$INPUT_STRING" = "Y" ]; then
  sudo apt-get install -y nginx
  sudo apt-get install -y mysql-server
  sudo apt-get install -y python-pip python-dev build-essential
  sudo apt-get install -y supervisor
  sudo apt-get install -y certbot python-certbot-nginx
fi
# sudo apt-get install -y default-libmysqlclient-dev # needed for python-mysql
python3 -m pip install --upgrade pip
python3 -m pip install Virtualenv
echo "Create a virtualenv in $ENVDIR"
if [ ! -d "$ENVDIR" ]; then
  # install python3 virtualenv
  mkdir -p $ENVDIR
  python3 -m virtualenv $ENVDIR
fi
# run in virtual env
# note source command does not work here
. "$ENVDIR/bin/activate"
pip install -r $DJANGO_DIR/project/deploy/conf/requirements.txt
