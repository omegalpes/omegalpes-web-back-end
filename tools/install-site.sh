#!/bin/sh#
# Site installer
#--------------------------ROOT_DIR=$(pwd)/.. # root dir is one level up to django
DJANGO_DIR=$(pwd)
DEPLOY_DIR=$DJANGO_DIR/project/deploy
PROJECT=backend-hubmkdir -p $ROOT/bin
mkdir -p $ROOT/var/run
mkdir -p $ROOT/var/log
mkdir -p $ROOT/var/log/supervisor# Nginx setup
sudo cp $ROOT/project/deploy/conf/nginx/sites-available/$PROJECT-live /etc/nginx/sites-available/$PROJECT
sudo ln -s /etc/nginx/sites-available/$PROJECT /etc/nginx/sites-enabled/$PROJECT
sudo service nginx restart# gunicorn setup
cp $ROOT/project/deploy/conf/gunicorn_start $ROOT/bin/# supervisor setup
#sudo cp $ROOT/project/deploy/conf/supervisor.conf /etc/supervisor/conf.d/$PROJECT.conf
sudo cp $ROOT/project/deploy/conf/supervisor/gunicorn.conf /etc/supervisor/conf.d
sudo cp $ROOT/project/deploy/conf/supervisor/mqtt-bridge.conf /etc/supervisor/conf.d
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl start $PROJECT # create certificate, this will modify /etc/nginx/sites-enabled/$PROJECT
#sudo certbot --cert-name cloud.cotherm.com# test with
#https://cloud.cotherm.com
#https://www.ssllabs.com/ssltest/analyze.html?d=cloud.cotherm.com
