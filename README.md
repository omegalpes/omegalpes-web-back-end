# OMEGAlpes-web-back-end

This project is made with the framework django and dango-rest-framework :
https://www.django-rest-framework.org/

The web server is made with Nginx, gunicorn, and surpervisor 

https://www.nginx.com/
https://gunicorn.org/
http://supervisord.org/

#Configuration pour exécution local
Pour executer en local :

```py manage.py runserver``` 

Allez à l'adresse : 
```http://127.0.0.1:8000/```


# Configuration Nginx, Gunicorn and supervisor : 

# Nginx : 


``` nginx
server {

        listen 80;
        server_name [your_serv];

        client_max_body_size 4G;

        access_log /home/omegalpes_adm/rec/omegalpes-web-back-end/var/log/nginx-access.log combined;
        error_log  /home/omegalpes_adm/rec/omegalpes-web-back-end/var/log/nginx-error.log;

        location / {
                 proxy_pass       http://127.0.0.1:8006;
                 proxy_set_header X-Real-IP $remote_addr;
                 proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                 proxy_set_header Host $http_host;
        }
}

```

# Gunicorn :

```bash
#!/bin/bash

NAME="omegalpes-web-back-end" # Name of the application

ROOTDIR=/home/omegalpes_adm/dev/$NAME
ENVDIR=$ROOTDIR/env
DJANGODIR=$ROOTDIR/server # Django project directory
SOCKFILE=$ROOTDIR/var/run/gunicorn.sock # we will communicte using this unix socket

USER=root # the user to run as
GROUP=root # the group to run as
NUM_WORKERS=3 # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=server.settings # which settings file should Django use
DJANGO_WSGI_MODULE=server.wsgi # WSGI module name
DJANGO_ACCESS_LOGFILE=$ROOTDIR/var/log/django_access.log
DJANGO_ERROR_LOGFILE=$ROOTDIR/var/log/django_error.log

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
. $ENVDIR/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec $ENVDIR/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
--name=$NAME \
--workers=$NUM_WORKERS \
--user=$USER --group=$GROUP \
--log-level=debug \
--access-logfile $DJANGO_ACCESS_LOGFILE \
--error-logfile $DJANGO_ERROR_LOGFILE \
--capture-output \
--enable-stdio-inheritance \
--bind=127.0.0.1:8005
```

# Supervisor :
```config
[program:omegalpes-dev-web-back-end]
command=/home/omegalpes_adm/dev/omegalpes-web-back-end/bin/gunicorn_start_dev
user=root
autostart=true
autorestart=true
stdout_logfile=/home/omegalpes_adm/dev/omegalpes-web-back-end/var/log/gunicorn_supervisor.log
stderr_logfile=/home/omegalpes_adm/dev/omegalpes-web-back-end/var/log/supervisor/%(program_name)s_stderr.log
```

# Tests
Go in folder `/server`
Run `python ./manage.py test -v 2` ( verboses testing ) or `python ./manage.py test`